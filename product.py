# This file is part product_barcode module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction

SHORT_NAME_LEN = 25


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @fields.depends('name', 'products')
    def on_change_name(self):
        if self.name:
            for product in self.products:
                product.short_name = self.name[:SHORT_NAME_LEN]


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    barcode = fields.Char('Barcode')
    short_name = fields.Char('Short Name', depends=['name'])
    tag = fields.Char('Tag')
    # rename table product.location to product.position

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()
        cls._order.insert(0, ('barcode', 'DESC'))

    @classmethod
    def create(cls, vlist):
        rec_ids = super(Product, cls).create(vlist)
        for rec in cls.browse(rec_ids):
            cls.write(
                [rec], {'short_name': rec.template.name[:SHORT_NAME_LEN]})
        return rec_ids

    @classmethod
    def copy(cls, records, default=None):
        cursor = Transaction().connection.cursor()
        if default is None:
            default = {}
        default = default.copy()
        code = str(default['template']) + '_copy'
        if default.get('template'):
            # products = cls.search([
            #     ('code', '!=', None),
            #     ('code', '!=', ''),
            # ], order=[('code', 'DESC')])
            # if products:
            #     print(products)
            #     str_code = products[0].code
            #     if str_code.isdigit():
            #         code = str(int(str_code) + 1).zfill(len(str_code))
            #     else:
            #         code = None
            cursor.execute(
                "SELECT max(code) from product_product where code != '' and code is not null")
            res = cursor.fetchall()
            code = ''
            if res:
                str_code = res[0][0]
                if str_code.isdigit():
                    code = str(int(str_code) + 1).zfill(len(str_code))
            default['code'] = code
        new_records = []
        for rec in records:
            new_rec, = super(Product, cls).copy(
                    [rec], default=default
                    )
            new_records.append(new_rec)
        return new_records

    def get_rec_name(self, name):
        get_rec_name = super(Product, self).get_rec_name(name)
        if self.barcode:
            return '[' + (self.code if self.code else '') + '] ' + self.name
        else:
            return get_rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Product, cls).search_rec_name(name, clause)
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                domain,
                ('barcode',) + tuple(clause[1:]),
                ]
